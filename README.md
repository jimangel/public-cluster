TODO: create scripts to add helm charts  
TODO: create scripts to generate manifests  
TODO: create scripts to copy "base" repo to sub-cluster repos  
TODO: create scripts to sync repo (images / other settings)
TODO: user below to explain repo

```
├── charts  
│   └── podinfo              # INFO  
│       ├── Chart.yaml  
│       ├── README.md 
│       ├── templates
│       └── values.yaml
├── hack                     # INFO
│   ├── Dockerfile.ci
│   └── ci-mock.sh
├── namespaces               # INFO
│   ├── dev.yaml
│   └── stg.yaml
└── releases                 # INFO 
    ├── dev
    │   └── podinfo.yaml
    └── stg
        └── podinfo.yaml
```

# install flux

```
# export KUBECONFIG="/path/to/kubeconfig.yaml"

# dir model: https://github.com/weaveworks/flux-get-started`
git clone https://github.com/weaveworks/flux
cd flux

# replace deploy/flux-deployment.yaml with your github repo:
        # replace or remove the following URL
        - --git-url=git@github.com:weaveworks/flux-get-started
        # - --git-url=git@bitbucket.org:jimangel/public-cluster.git
        - --git-branch=master
        
kubectl apply -f deploy
# serviceaccount/flux created
# clusterrole.rbac.authorization.k8s.io/flux created
# clusterrolebinding.rbac.authorization.k8s.io/flux created
# deployment.apps/flux created
# secret/flux-git-deploy created
# deployment.apps/memcached created
# service/memcached created

# Allow some time for all containers to get up and running. If you're impatient, run the following command and see the pod creation process.
# watch kubectl get pods --all-namespaces

# install fluxctl
# https://github.com/weaveworks/flux/blob/master/site/fluxctl.md

# get ssh and add to bitbucket
fluxctl identity

# make sure to add this to the user account NOT THE REPO...


```
